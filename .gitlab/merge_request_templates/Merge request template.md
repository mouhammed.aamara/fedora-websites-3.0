# Description of merge request

## Characteristics of merge request

- [ ] Bug fix
- [ ] New feature
- [ ] Breaking change
- [ ] Documentation update required

## Was a static build tested locally with `npm run generate`?

- [ ] Yes
- [ ] No
